# PDP Wordpress #

Portale dell'associazione PDP - Free Software User Group

## How to use ##
Type:

    $ docker-compose up

and go to `http://localhost/` on your browser

### For deploy on server ###
Open `dump.sql` and replace `http://127.0.0.1/` with your domain (2 occurrences)
